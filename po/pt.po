# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the gnome-shell package.
#
# Translators:
# Adorilson Bezerra <adorilson@gmail.com>, 2011
# António Lima <amrlima@gmail.com>, 2013
# Bruno Ramalhete <bram.512@gmail.com>, 2015
# Djavan Fagundes <djavan@comum.org>, 2012
# Duarte Loreto <happyguy_pt@hotmail.com>, 2010-2014
# Enrico Nicoletto <liverig@gmail.com>, 2013
# Felipe Borges <felipe10borges@gmail.com>, 2010
# Gabriel F. Vilar <cogumm@gmail.com>, 2011
# Henrique P. Machado <hpmachado@gnome.org>, 2010-2011
# Joel Gil León <joel@endlessm.com>, 2013-2014
# Jonh Wendell <wendell@bani.com.br>, 2010
# Og Maciel <ogmaciel@gnome.org>, 2009
# Pedro Albuquerque <palbuquerque73@gmail.com>, 2014-2015
# Rafael Fontenelle <rffontenelle@gmail.com>, 2013
# Rafael Fontenelle <rffontenelle@gmail.com>, 2013
# Renato Peixoto Pinto <renato@endlessm.com>, 2013-2014
# Ricardo Pitanga <pitanga@endlessm.com>, 2013
# Richard Vignais, 2014
# Richard Vignais, 2014
# Roddy Shuler <roddy@stanfordalumni.org>, 2013-2015
# Rodrigo Flores <mail@rodrigoflores.org>, 2009
# Rodrigo Padula <contato@rodrigopadula.com>, 2011
# Rui Gouveia <rui.gouveia@gmail.com>, 2011
# Sérgio Cardeira <cardeira dot sergio at gmail dot com>, 2016
# Silva <michele@endlessm.com>, 2014-2015
msgid ""
msgstr ""
"Project-Id-Version: gnome-shell\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-09-25 22:24+0100\n"
"PO-Revision-Date: 2020-04-24 13:19+0000\n"
"Last-Translator: Vicki Niu <vicki@endlessm.com>\n"
"Language-Team: Portuguese (http://www.transifex.com/endless-os/gnome-shell/"
"language/pt/)\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: extension.js:67
msgid "Quit applications"
msgstr ""

#: extension.js:68
msgid ""
"If an application doesn't respond for a while, select its name and click "
"Quit Application."
msgstr ""

#: extension.js:87
msgid "Cancel"
msgstr "Cancelar"

#: extension.js:95
msgid "System Monitor"
msgstr ""

#: extension.js:101
msgid "Quit Application"
msgstr ""
